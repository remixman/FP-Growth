#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <memory>
#include <set>
#include <vector>
#include <utility>
#include <limits>
#include <cmath>
#include <cstdlib>
using namespace std;

#define all(x) x.begin(),x.end()

/* Config data */
#define MINIMUM_SUPPORT 2
#define HIERARCHY_FILE_1 "../transaction_hierarchy_1.txt"
#define HIERARCHY_FILE_2 "../transaction_hierarchy_2.txt"
#define HIERARCHY_FILE_3 "../transaction_hierarchy_3.txt"
#define TRANSACTION_FILE "../new_transactions.txt"
//#define AGGREGATE_OPERATOR //

/* Aggregate support method */
#define AGG_AVG 1
#define AGG_UNION 2
const int aggregate_method = AGG_UNION;

typedef vector<string> Transaction;
typedef vector< pair<string, float> > conditional_fp_tree_record;
typedef vector< pair< string,conditional_fp_tree_record > > conditional_fp_tree;

class Row_Data {
public:
    Row_Data(int max_level) {
        label = "";
        this->max_level = max_level;
        support_count_leaf.reset(new float[max_level]);
        for (int i = 0; i < max_level; ++i)
            support_count_leaf[i] = 0.0f;
    }
    void print_data() const {
        printf("%-5s", label.c_str());
        for (int i = 0; i < max_level; i++)
            printf("%-8.3lf", support_count_leaf[i]);
        printf("\n");
    }
    string label;
    int max_level;
    unique_ptr<float []> support_count_leaf;
    // TODO: node head
    // TODO: node tail
};

class Hierarchy_Node {
public:
    Hierarchy_Node(string label) {
        this->label = label;
        parent = NULL;
    }
    void add_child(Hierarchy_Node * node) {
        childs.push_back(node);
    }
    string label;
    Hierarchy_Node * parent;
    vector< Hierarchy_Node* > childs;
};

class Hierarchy {
public:
    Hierarchy(string filename) {
        max_level = 0;
        support_count = 0.f;
        init_from_file(filename);
    }
    void init_from_file(string filename) {
        ifstream file;
        file.open(filename.c_str(), ifstream::in);

        if (!file) {
            cerr << "Cannot open file " << filename << endl;
            return;
        }
        root = create_tree(file);
        file.close();
    }
    Hierarchy_Node * create_tree(ifstream &file, size_t level = 0) {
        string name; int child_num;
        file >> name >> child_num;
        Hierarchy_Node * node = new Hierarchy_Node(name);

        if (level + 1 > max_level) max_level = level + 1;

        // Save to index
        quick_access_map[name] = node;
        if (node_in_level.size() < level+1) {
            vector<Hierarchy_Node *> new_list;
            node_in_level.push_back( new_list );
        }
        node_in_level[level].push_back(node);

        // Call create tree recursively
        for (int i = 0; i < child_num; ++i) {
            node->add_child( create_tree(file, level+1) );
            node->childs[i]->parent = node;
        }

        return node;
    }
    bool is_in(string a) const {
        return quick_access_map.find(a) != quick_access_map.end();
    }
    bool in_same_group(string a, string b) const {
        const auto& anode = quick_access_map.at(a);
        const auto& bnode = quick_access_map.at(b);
        return anode->parent == bnode->parent;
    }
    string get_group_name(string a) const {
        const auto& anode = quick_access_map.at(a);
        if (anode->parent)
            return anode->parent->label;
        else
            return "";
    }

    void print_tree() const {
        vector<bool> has_next_child;
        print_tree_rec(root, 0, has_next_child);
    }
    void print_tree_rec(Hierarchy_Node *node, int indent, vector<bool>& has_next_child) const {
        if (indent > 0) {
            for (int i = 0; i < indent-1; i++) {
                if (has_next_child[i])
                    cout << "|    ";
                else
                    cout << "     ";
            }
            cout << "|__";
        }
        cout << "(" << node->label << ")" << endl;
        has_next_child.push_back(false);
        size_t i = 0;
        for (auto& child : node->childs) {
            has_next_child[has_next_child.size() - 1] = (i != node->childs.size()-1);
            print_tree_rec(child, indent + 1, has_next_child);
            ++i;
        }
        has_next_child.pop_back();
    }
    
    float support_count;
    Hierarchy_Node * root;
    size_t max_level;
    map<string, Hierarchy_Node*> quick_access_map;
    vector< vector<Hierarchy_Node *> > node_in_level;
};

class FP_Tree_Node {
public:
    FP_Tree_Node() {
        label = "";
        frequency = 0;
        reduced = false;
    }

    string label;
    float frequency;
    bool reduced;
    shared_ptr<FP_Tree_Node> parent;
	map<string,shared_ptr<FP_Tree_Node>> childs;
    shared_ptr<FP_Tree_Node> next_link;
};

vector<Hierarchy> hierarchies;
string get_group_name(string a) {
    for (auto &h : hierarchies)
        if (h.is_in(a)) return h.get_group_name(a);
    return "";
}
bool same_group(string a, string b) {
    for (auto &h : hierarchies) {
        if (h.is_in(a) && h.is_in(b)) {
            return h.in_same_group(a, b);
        }
    }
    return false;
}


class FP_Tree {
public:
    FP_Tree(vector< Transaction > &transaction_list) {
        root.reset( new FP_Tree_Node() );
        for (auto& transaction : transaction_list) {
            shared_ptr<FP_Tree_Node> cur_node = root;

            cout << "SORTED TRANSACTION : " ;
            for (auto& item : transaction) {
                // Create new node
                cout << item << " ";
                if (cur_node->childs.find(item) == cur_node->childs.end()) {
                    shared_ptr<FP_Tree_Node> child_node(new FP_Tree_Node());
                    child_node->parent = cur_node;
                    child_node->label = item;
                    cur_node->childs[item] = child_node;

                    append_to_node_list(child_node);
                }

                shared_ptr<FP_Tree_Node> node = cur_node->childs[item];
                node->frequency += 1;

                cur_node = node;
            }
            cout << endl;
        }
    }

    void group_node() {
        group_node_rec(root, nullptr, nullptr);
    }

    size_t group_node_rec(shared_ptr<FP_Tree_Node> node, shared_ptr<FP_Tree_Node> parent,
            vector< shared_ptr<FP_Tree_Node> > *new_children) {
        int same_group_count = 0;

        if (node->label != "" && get_group_name(node->label) != "") {
            for (auto& child : node->childs)
                if (same_group(node->label, child.second->label))
                    same_group_count++;
                else if (new_children != NULL)
                    new_children->push_back(child.second);
        }

        // Group children recursively
        if (same_group_count == 0) {
            for (auto& child : node->childs)
                group_node_rec(child.second, node, NULL);
            return node->frequency;
        }

        // Collect node in same group and children
        if (new_children == NULL)
            new_children = new vector<shared_ptr<FP_Tree_Node>>();
        size_t group_min_frequent = numeric_limits<size_t>::max();
        size_t group_sum_frequent = 0;
        for (auto& child : node->childs) {
            if (same_group(node->label, child.second->label)) {
                size_t freq = group_node_rec(child.second, node, new_children);
                if (freq < group_min_frequent) group_min_frequent = freq;
                group_sum_frequent += freq;
            }
        }

        // Create grouping node
        shared_ptr<FP_Tree_Node> new_node(new FP_Tree_Node());
        new_node->label = get_group_name(node->label);
        new_node->frequency = group_sum_frequent; //group_min_frequent;
        new_node->parent = parent;
        for (auto& new_child : *new_children)
            new_node->childs[new_child->label] = new_child;

        delete new_children;
        append_to_node_list(new_node);

        // Add to parent node
        parent->childs[new_node->label] = new_node;

        // Grouping new children
        for (auto& child : new_node->childs)
            group_node_rec(child.second, new_node, NULL);

        return node->frequency;
    }

    void generate_frequent_pattern() {
        // Condition pattern base

        // map< string, pair<FP_Tree_Node*, FP_Tree_Node*> > node_links;
        for (auto& node_link : node_links) {
            string label = node_link.first;
            vector< pair<set<string>, float> > item_patterns;
            vector< pair<string, float> > item_cond_fp;
            shared_ptr<FP_Tree_Node> node = node_link.second.first;
            map<string, float> item_count_map;

            while (node) {
                set<string> item_set;
                shared_ptr<FP_Tree_Node> parent = node->parent;
                float frequency = node->frequency;

                while (parent->label != "") {
                    string& item = parent->label;
                    item_set.insert(item);

                    item_count_map[item] += frequency;
                    parent = parent->parent;
                }

                if (item_set.size() > 0) {
                    auto pattern = make_pair(item_set, node->frequency);
                    item_patterns.push_back(pattern);
                }

                node = node->next_link; // Fetch next branch
            }

            for (const auto& item_count : item_count_map) {
                if (item_count.second >= MINIMUM_SUPPORT) {
                    item_cond_fp.push_back(item_count);
                }
            }

            if (item_count_map.size() > 0) {
                cond_fp_tree.push_back( make_pair(label, item_cond_fp) );
            }
        }


        // Debug
        for (const auto& cond_fp_row : cond_fp_tree) {
            cout << "Item [" << cond_fp_row.first << "] : ";
            for (const auto item_count : cond_fp_row.second) {
                cout << "(" << item_count.first << ":" << item_count.second << ") ";
            }
            cout << endl;
        }
    }

    void generate_rule() {
        for (const auto& cond_fp_row : cond_fp_tree) {
            auto& item = cond_fp_row.first;
            auto& co_occur_items = cond_fp_row.second;
            int co_occur_num = co_occur_items.size();
            int subset_num = pow(2, co_occur_num);

            // Generate subsets
            for (int x = 1; x < subset_num; ++x) {
                int bit = x;
                int support = 0;
                for (int i = 0; i < co_occur_num; ++i) {
                    if (bit & 1) {
                        cout << co_occur_items[i].first << ",";
                        support = co_occur_items[i].second;
                    }

                    bit >>= 1;
                }
                cout << "\b -> " << item << "\t\tsupport = " << support << endl;
            }
        }
    }

    void print_tree() const {
        vector<bool> has_next_child;
        print_tree_rec(root, 0, has_next_child);
    }

    void print_tree_rec(shared_ptr<FP_Tree_Node> node, int indent, vector<bool>& has_next_child) const {
        if (indent > 0) {
            for (int i = 0; i < indent-1; i++) {
                if (has_next_child[i])
                    cout << "|    ";
                else
                    cout << "     ";
            }
            cout << "|__";
        }
        cout << "(" << node->label << ":" << node->frequency << ")" << endl;
        has_next_child.push_back(false);
        size_t i = 0;
        for (auto& child : node->childs) {
            has_next_child[has_next_child.size() - 1] = (i != node->childs.size()-1);
            print_tree_rec(child.second, indent + 1, has_next_child);
            ++i;
        }
        has_next_child.pop_back();
    }

    void append_to_node_list(shared_ptr<FP_Tree_Node> node) {
        string& item = node->label;
        if (node_links.find(item) == node_links.end()) {
            auto link_pair = make_pair(node, node);
            node_links[item] = link_pair;
        } else {
            shared_ptr<FP_Tree_Node> tail = node_links[item].second;
            tail->next_link = node;
            node_links[item].second = node;
        }
    }

    shared_ptr<FP_Tree_Node> root;
    map< string, pair<shared_ptr<FP_Tree_Node>, shared_ptr<FP_Tree_Node>> > node_links; // (item_name, link head, link tail)
    conditional_fp_tree cond_fp_tree;
};

// return <support,node_num>
pair<float, int> count_all_support(map<string, Row_Data*>& row_data_map,
        Hierarchy_Node *node, size_t max_level) {
    // Base case
    if (node->childs.size() == 0) {
        Row_Data * row_data = row_data_map[node->label];
        if (!row_data) return make_pair(0, 0);
        return make_pair(row_data->support_count_leaf[0], 1);
    }

    float support_sum(0.0f);
    int node_num_sum(0);
    for (auto& child : node->childs) {
        auto count_res = count_all_support(row_data_map, child, max_level);
        support_sum += count_res.first;
        node_num_sum += count_res.second;
    }

    return make_pair(support_sum, node_num_sum);
}

void find_leaf_item_list(set<string> &leaf_item_list, Hierarchy_Node *node) {
    if (node->childs.size() == 0) {
        leaf_item_list.insert(node->label);
        return;
    }

    for (auto& child : node->childs)
        find_leaf_item_list(leaf_item_list, child);
}

void set_support(map<string, Row_Data*>& row_data_map, Hierarchy_Node *node,
        int level, float support) {
    // Base case
    if (node->childs.size() == 0) {
        Row_Data * row_data = row_data_map[node->label];
        if (!row_data) return;
        row_data->support_count_leaf[level] = support;
        return;
    }

    for (auto& child : node->childs)
        set_support(row_data_map, child, level, support);
}

void count_support_count(const vector< vector<string> >& tran_list,
        map<string, Row_Data*> &row_data_map /* output */,
        vector< Row_Data* > &node_list /* output */, Hierarchy& hierarchy) {

    int max_level = hierarchy.max_level;

    // Create node list row from transaction list
    // and set most leaf support count
    for (auto& tran : tran_list) {
        for (auto& item : tran) {
            if (row_data_map.find(item) == row_data_map.end()) {
                Row_Data *row_data = new Row_Data(max_level);
                row_data->label = item;
                row_data->support_count_leaf[0] = 0;
                row_data_map[item] = row_data;
                node_list.push_back(row_data);
            }
            row_data_map[item]->support_count_leaf[0] += 1;
        }
    }

    /*for (auto& dat : row_data_map) {
        cout << dat.first << " - " << dat.second->support_count_leaf[0] << endl;
    }*/

    // Calculate support count for lower level
    if (aggregate_method == AGG_AVG) {
        float max_support = 0;
        int leaf_level = 0, rev_level = max_level - 1;
        for (; leaf_level < max_level; ++leaf_level, --rev_level) {
            auto& level_node_list = hierarchy.node_in_level[rev_level];
            for (auto node : level_node_list) {
                auto count_res = count_all_support(row_data_map, node, max_level);
                float avg = count_res.first / count_res.second;
                set_support(row_data_map, node, leaf_level, avg);
                if (avg > max_support) max_support = avg;
            }
        }
        hierarchy.support_count = max_support;
    } else {
        vector<pair<Row_Data*,int>> leaf_gt_0; 
        int leaf_level = 0, rev_level = max_level - 1;
        for (; leaf_level < max_level; ++leaf_level, --rev_level) {
            auto& level_node_list = hierarchy.node_in_level[rev_level];
            for (auto node : level_node_list) {
                set<string> leaf_item_list;
                find_leaf_item_list(leaf_item_list, node);
                if (*(leaf_item_list.begin()) == node->label && leaf_level > 0) {
                    Row_Data* row = row_data_map[node->label];
                    leaf_gt_0.push_back( make_pair(row, leaf_level) );
                }
                // count support in transaction
                int support = 0;
                for (auto tran : tran_list) {
                    for (auto item : tran) {
                        if (leaf_item_list.count(item) > 0) {
                            support += 1;
                            break; // go to next transaction
                        }
                    }
                }
                set_support(row_data_map, node, leaf_level, support);
            }
        }
        
        // for all leaf node that level is not 0
        for (auto nl : leaf_gt_0) {
            Row_Data *row = nl.first;
            if (row == NULL) continue;
            int level = nl.second;
            for (int i = 0; i < (max_level - level - 1); ++i)
                row->support_count_leaf[i] = row->support_count_leaf[i+level];
        }
        
    }

}

void sort_node_list(vector< Row_Data* >& node_list, const Hierarchy& hierarchy) {
    int leaf_level;
    auto sort_leaf_level_function = [](Row_Data* r1, Row_Data* r2) {
        if (r1->support_count_leaf[0] > r2->support_count_leaf[0])
            return true;
        else if (r1->support_count_leaf[0] == r2->support_count_leaf[0])
            return r1->label > r2->label;
        else
            return false;
    };

    auto sort_other_level_function = [&leaf_level](Row_Data* r1, Row_Data* r2) {
        int l = leaf_level;
        if (r1->support_count_leaf[l] > r2->support_count_leaf[l])
            return true;
        else
            return false;
    };

    for (size_t ll = 0; ll < hierarchy.max_level; ++ll) {
        leaf_level = ll;
        if (ll == 0)
            stable_sort(node_list.begin(), node_list.end(), sort_leaf_level_function);
        else
            stable_sort(node_list.begin(), node_list.end(), sort_other_level_function);
    }
}

float get_average_support_count(const vector< Row_Data > node_list) {
    float node_num = node_list.size();
    float support_sum = 0;
    for (const auto& node : node_list)
        support_sum += node.support_count_leaf[0];
    return support_sum / node_num;
}

int main() {

    // 1. Read hierarchy information
    hierarchies.push_back(Hierarchy(HIERARCHY_FILE_1));
    hierarchies.push_back(Hierarchy(HIERARCHY_FILE_2));
    hierarchies.push_back(Hierarchy(HIERARCHY_FILE_3));
    printf("Phase 1 : Hierarchy\n");
    for_each(all(hierarchies), [](const Hierarchy& h) { h.print_tree(); });
    printf("=============================================\n");

    // 2. Read transaction data - p1 p2 ... pn b1 b2 ... bm v1 v2 ... vk
    ifstream transaction_file;
    transaction_file.open(TRANSACTION_FILE, ifstream::in);
    if (!transaction_file) {
        cerr << "Cannot open file" << endl;
        return 0;
    }

    vector< Transaction > transaction_list;
    while (!transaction_file.eof()) {
        int n;
        Transaction tran;
        transaction_file >> n;
        if (n == 0) break;
        tran.resize(n);
        for (int i = 0; i < n; ++i) transaction_file >> tran[i];

        transaction_list.push_back(tran);
    }
    transaction_file.close();

    // 3. Create table
    vector< Row_Data* > item_node_list;
    vector< decltype(item_node_list) > node_lists( hierarchies.size() );
    map<string, Row_Data*> row_data_map;
    for (int i = 0; i < 3; ++i) {
        count_support_count(transaction_list, row_data_map, node_lists[i], hierarchies[i]);
        // /cout << "MAX LV : " << node_lists[i][0]->max_level << endl;
    }

    // 4. Sort table
    for (int i = 0; i < 3; ++i) {
        sort_node_list(node_lists[i], hierarchies[i]);
        
    }
        
    for (int i = 0; i < 3 - 1; ++i) {
        for (int k = i + 1; k < 3; ++k) {
            if (hierarchies[k].support_count > hierarchies[i].support_count) {
                swap(hierarchies[i], hierarchies[k]);
                swap(node_lists[i], node_lists[k]);
            }
        }
    }
    int order_counter = 0;
    map<string, int> table_order;
    for (int i = 0; i < 3; ++i) {
        for (auto row_data : node_lists[i]) {
            item_node_list.push_back(row_data);
            table_order[row_data->label] = ++order_counter;
        }
            
    }

    printf("Phase 2 : Sort by support count\n");
    for (auto& data : item_node_list) data->print_data();
    printf("=============================================\n");

    // 5. Sort data in each transaction
    size_t hierachy_level = 1<<30;
    for (int i = 0; i < 3; ++i) {
        if (hierarchies[i].max_level < hierachy_level)
            hierachy_level = hierarchies[i].max_level;
    }
    for (auto& transaction : transaction_list) {
        sort(all(transaction), [&table_order](string a, string b) {
            return table_order[a] < table_order[b];
        });
    }

    // 6. Built FP tree
    FP_Tree fp_tree(transaction_list);
    printf("Phase 3 : Build FP Tree\n");
    fp_tree.print_tree();
    printf("=============================================\n");

    // 7. Group subcategory
    fp_tree.group_node();
    printf("Phase 4 : Group category and add new node on tree\n");
    fp_tree.print_tree();
    printf("=============================================\n");

    // 8. Generate rules
    printf("Phase 5 : Generate rules\n");
    fp_tree.generate_frequent_pattern();
    printf("=============================================\n");

    // 9. Generate rules
    printf("Phase 6 : Generate rules\n");
    fp_tree.generate_rule();
    printf("=============================================\n");

    return 0;
}
