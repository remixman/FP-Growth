#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <utility>
#include <cmath>
using namespace std;

const int minimum_support = 2;

class FP_Tree_Node;

class Item {
public:
    Item() {
        support_count = 0;
        node_link_head = NULL;
        node_link_tail = NULL;
    }
    string item_name;
    int support_count;
    FP_Tree_Node * node_link_head;
    FP_Tree_Node * node_link_tail;
};

class FP_Tree_Node {
public:
    FP_Tree_Node() {
        label = "";
        frequency = 0;
        parent = NULL;
        next_link = NULL;
    }
    ~FP_Tree_Node() {
        for (auto& child : childs)
            delete child.second;
    }

    string label;
    int frequency;
    FP_Tree_Node * parent;
	map<string, FP_Tree_Node *> childs;
    FP_Tree_Node * next_link;
};

void print_tree_rec(FP_Tree_Node * node, int indent, vector<bool>& has_next_child) {
    if (indent > 0) {
        for (int i = 0; i < indent-1; i++) {
            if (has_next_child[i])
                cout << "|    ";
            else
                cout << "     ";
        }
        cout << "|__";
    }
    cout << "(" << node->label << ":" << node->frequency << ")" << endl;
    has_next_child.push_back(false);
    int i = 0;
    for (auto& child : node->childs) {
        has_next_child[has_next_child.size() - 1] = (i != node->childs.size()-1);
        print_tree_rec(child.second, indent + 1, has_next_child);
        ++i;
    }
    has_next_child.pop_back();
}

void print_tree(FP_Tree_Node * node) {
    vector<bool> has_next_child;
    print_tree_rec(node, 0, has_next_child);
}

int main(int argc, char *argv[]) {

    int n;
    ifstream transaction_file;
    transaction_file.open("transactions2.txt", ifstream::in);

    if (!transaction_file) {
        cerr << "Cannot open file " << endl;
        return 0;
    }

    // 1. Open file read transaction
    vector< vector<string> > transaction_list;
    while (!transaction_file.eof()) {
        vector<string> tran;
        transaction_file >> n;
        if (n == 0) break;
        //cout << n << endl;
        for (int i = 0; i < n; i++) {
            string item;
            transaction_file >> item;
            //cout << "\t" << item << endl;
            tran.push_back(item);
        }
        transaction_list.push_back(tran);
    }

    transaction_file.close();

    // 2. Create table
    map<string, int> support_counts;
    for (auto& transaction : transaction_list)
        for (auto& item : transaction)
            support_counts[item] += 1;

    vector<Item> items_tree;
    for (auto& item_count : support_counts) {
        Item item;
        item.item_name = item_count.first;
        item.support_count = item_count.second;
        items_tree.push_back(item);
        //cout << item.item_name << " - " << item.support_count << endl;
    }

    // 3. Sort table
    auto sort_item_tree_func = [](Item a, Item b) {
        if (a.support_count > b.support_count)        return true;
        else if (a.support_count == b.support_count)  return a.item_name > b.item_name;
        else                                          return false;
    };
    sort(items_tree.begin(), items_tree.end(), sort_item_tree_func);
    for (auto& item : items_tree)
        cout << item.item_name << " - " << item.support_count << endl;

    map< string, pair<FP_Tree_Node*, FP_Tree_Node*> > node_links; // (item_name, link head, link tail)
    auto append_to_node_list = [&node_links](FP_Tree_Node * node) {
        string& item = node->label;
        if (node_links.find(item) == node_links.end()) {
            pair<FP_Tree_Node*,FP_Tree_Node*> link_pair = make_pair(node, node);
            node_links[item] = link_pair;
        } else {
            FP_Tree_Node * tail = node_links[item].second;
            tail->next_link = node;
            node_links[item].second = node;
        }
    };

    // 4. Create tree
    FP_Tree_Node * root = new FP_Tree_Node();
    auto sort_item_func = [&support_counts](string a, string b) {
        if (support_counts[a] > support_counts[b])       return true;
        else if (support_counts[a] == support_counts[b]) return a > b;
        else                                             return false;
    };
    for (auto& transaction : transaction_list) {
        // Sort item in transaction
        sort(transaction.begin(), transaction.end(), sort_item_func);
        FP_Tree_Node * cur_node = root;
        for (auto& item : transaction) {
            // Create new node
            if (cur_node->childs.find(item) == cur_node->childs.end()) {
                FP_Tree_Node * child_node = new FP_Tree_Node();
                child_node->parent = cur_node;
                child_node->label = item;
                cur_node->childs[item] = child_node;

                append_to_node_list(child_node);
            }

            FP_Tree_Node * node = cur_node->childs[item];
            node->frequency += 1;

            cur_node = node;
        }
    }
    print_tree(root);

    // 5. Generate condition pattern
    reverse(items_tree.begin(), items_tree.end());
    typedef vector< pair<string,int> > conditional_fp_tree_record;
    typedef vector< pair< string,conditional_fp_tree_record > > conditional_fp_tree;
    conditional_fp_tree cond_fp_tree;
    for (auto& item : items_tree) {
        FP_Tree_Node * node = node_links[item.item_name].first;
        vector< pair<set<string>, int> > item_patterns;
        conditional_fp_tree_record item_cond_fp;

        map<string, int> item_count_map;
        // For each branch
        while (node) {
            set<string> item_set;
            FP_Tree_Node * parent = node->parent;
            int frequency = node->frequency;
            while (parent->label != "") {
                string& item = parent->label;
                item_set.insert(item);

                item_count_map[item] += frequency;

                parent = parent->parent;
            }

            if (item_set.size() > 0) {
                auto pattern = make_pair(item_set, node->frequency);
                item_patterns.push_back(pattern);
            }

            node = node->next_link; // Fetch next branch
        }

        for (const auto& item_count : item_count_map) {
            if (item_count.second >= minimum_support) {
                item_cond_fp.push_back(item_count);
            }
        }

        if (item_count_map.size() > 0) {
            cond_fp_tree.push_back( make_pair(item.item_name,item_cond_fp) );
        }

    }

    for (const auto& cond_fp_row : cond_fp_tree) {
        cout << "Item [" << cond_fp_row.first << "] : ";
        for (const auto item_count : cond_fp_row.second) {
            cout << "(" << item_count.first << ":" << item_count.second << ") ";
        }
        cout << endl;
    }


    // 6. Generate rule
    for (const auto& cond_fp_row : cond_fp_tree) {
        auto& item = cond_fp_row.first;
        auto& co_occur_items = cond_fp_row.second;
        int co_occur_num = co_occur_items.size();
        int subset_num = pow(2, co_occur_num);

        // Generate subsets
        for (int x = 1; x < subset_num; ++x) {
            int bit = x;
            int support = 0;
            for (int i = 0; i < co_occur_num; ++i) {
                if (bit & 1) {
                    cout << co_occur_items[i].first << ",";
                    support = co_occur_items[i].second;
                }

                bit >>= 1;
            }
            cout << "\b -> " << item << "\t\tsupport = " << support << endl;
        }
    }



    // X. Deallocate tree
    delete root;

	return 0;
}
